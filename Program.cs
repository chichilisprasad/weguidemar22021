﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
Modified
Test
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string secret = CognitoHashCalculator.GetSecretHash("22496860108891602049611713", "1j1stflnc8pd8ninmbmu7d0us6", "ci0rf9hijidtjo7r2jg64tonlvsjtcu3t0jsn2u2cbqchssvhnl");
        }
    }
    public static class CognitoHashCalculator
    {
        public static string GetSecretHash(string username, string appClientId, string appSecretKey)
        {
            var dataString = username + appClientId;

            var data = Encoding.UTF8.GetBytes(dataString);
            var key = Encoding.UTF8.GetBytes(appSecretKey);

            return Convert.ToBase64String(HmacSHA256(data, key));
        }

        public static byte[] HmacSHA256(byte[] data, byte[] key)
        {
            using (var shaAlgorithm = new System.Security.Cryptography.HMACSHA256(key))
            {
                var result = shaAlgorithm.ComputeHash(data);
                return result;
            }
        }
    }
}
